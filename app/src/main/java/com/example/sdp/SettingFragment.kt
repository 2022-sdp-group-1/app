package com.example.sdp

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.navigation.fragment.findNavController
import com.example.sdp.R


class SettingFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val view = inflater.inflate(R.layout.fragment_setting, container, false)
        val button_bluetooth = view.findViewById<Button>(R.id.bluetooth)
        val button_contact = view.findViewById<Button>(R.id.contact)
        val button_terms = view.findViewById<Button>(R.id.terms)

        button_bluetooth.setOnClickListener {
            findNavController().navigate(R.id.action_id_setting_fragment_to_setting_bluetoothFragment)

        }

        button_contact.setOnClickListener {
            findNavController().navigate(R.id.action_id_setting_fragment_to_setting_contactFragment)
        }

        button_terms.setOnClickListener{
            findNavController().navigate(R.id.action_id_setting_fragment_to_setting_termsFragment)
        }
        return view

    }

}